// All the mcufriend.com UNO shields have the same pinout.
// i.e. control pins A0-A4.  Data D2-D9.  microSD D10-D13.
// Touchscreens are normally A1, A2, D7, D6 but the order varies
//
// This demo should work with most Adafruit TFT libraries
// If you are not using a shield,  use a full Adafruit constructor()
// e.g. Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0
#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// letters in my font
#define blank5 0
#define charLetterA 1
#define charLetteru 2
#define charLettert 3
#define charLettero 4
#define charLetterC 5
#define charLetterH 6
#define charLetteri 7
#define charLetterL 8
#define charDegree 9
#define charNumeral0 10
#define charNumeral1 11
#define charNumeral2 12
#define charNumeral3 13
#define charNumeral4 14
#define charNumeral5 15
#define charNumeral6 16
#define charNumeral7 17
#define charNumeral8 18
#define charNumeral9 19
#define charFanA1 20
#define charFanA2 21
#define charFanA3 22
#define charFanB1 23
#define charFanB2 24
#define charFanB3 25
#define charScreenA1 26
#define charScreenA2 27
#define charScreenA3 28
#define charScreenB1 29
#define charScreenB2 30
#define charScreenB3 31
#define charFaceA1 32
#define charFaceA2 33
#define charFaceA3 34
#define charFaceB1 35
#define charFaceB2 36
#define charFaceB3 37
#define charFootA1 38
#define charFootA2 39
#define charFootA3 0
#define charFootB1 40
#define charFootB2 41
#define charFootB3 42
#define charArrowRA1 43
#define charArrowRA2 44
#define charArrowRA3 45
#define charArrowRB1 46
#define charArrowRB2 47
#define charArrowRB3 48
#define charArrowRDA1 49
#define charArrowRDA2 50
#define charArrowRDA3 51
#define charArrowRDB1 0
#define charArrowRDB2 52
#define charArrowRDB3 53
#define charPersonA1 0
#define charPersonA2 0
#define charPersonA3 0
#define charPersonA4 0
#define charPersonA5 59
#define charPersonA6 60
#define charPersonB1 0
#define charPersonB2 0
#define charPersonB3 0
#define charPersonB4 61
#define charPersonB5 62
#define charPersonB6 63
#define charPersonC1 0
#define charPersonC2 0
#define charPersonC3 64
#define charPersonC4 65
#define charPersonC5 66
#define charPersonC6 67
#define charPersonD1 0
#define charPersonD2 68
#define charPersonD3 69
#define charPersonD4 70
#define charPersonD5 71
#define charPersonD6 72
#define charPersonE1 73
#define charPersonE2 74
#define charPersonE3 0
#define charPersonE4 0
#define charPersonE5 0
#define charPersonE6 0
#define charPersonF1 75
#define charPersonF2 76
#define charPersonF3 0
#define charPersonF4 0
#define charPersonF5 0
#define charPersonF6 0
#define charDecimal 77

// #define DO_TESTS
// #define DO_RABBIT

#include <SPI.h>          // f.k. for Arduino-1.5.2
#include "Adafruit_GFX.h"// Hardware-specific library
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;


//#include <Adafruit_TFTLCD.h>
//Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F // green in a bitmap!
#define	RED     0xF800 // blue in a bitmap!
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GREY    0x7BEF

#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

void setup(void);
void loop(void);
void testloop(void);
unsigned long testFillScreen();
unsigned long testText();
unsigned long testLines(uint16_t color);
unsigned long testFastLines(uint16_t color1, uint16_t color2);
unsigned long testRects(uint16_t color);
unsigned long testFilledRects(uint16_t color1, uint16_t color2);
unsigned long testFilledCircles(uint8_t radius, uint16_t color);
unsigned long testCircles(uint8_t radius, uint16_t color);
unsigned long testTriangles();
unsigned long testFilledTriangles();
unsigned long testRoundRects();
unsigned long testFilledRoundRects();
void progmemPrint(const char *str);
void progmemPrintln(const char *str);

void runtests(void);

uint16_t g_identifier;

uint16_t iconWid = 40;
uint16_t iconHig = 40;
uint16_t iconHpos = 120;
uint16_t iconVpos = 100;


uint8_t lookupChar(char asciichar) {
  uint8_t returnValue;
  switch (asciichar) {
    case '0' :
      returnValue = charNumeral0;
      break;
    case '1' :
      returnValue = charNumeral1;
      break;
    case '2' :
      returnValue = charNumeral2;
      break;
    case '3' :
      returnValue = charNumeral3;
      break;
    case '4' :
      returnValue = charNumeral4;
      break;
    case '5' :
      returnValue = charNumeral5;
      break;
    case '6' :
      returnValue = charNumeral6;
      break;
    case '7' :
      returnValue = charNumeral7;
      break;
    case '8' :
      returnValue = charNumeral8;
      break;
    case '9' :
      returnValue = charNumeral9;
      break;
    case 'H' :
      returnValue = charLetterH;
      break;
    case 'L' :
      returnValue = charLetterL;
      break;
    case 'A' :
      returnValue = charLetterL;
      break;
    case 'u' :
      returnValue = charLetteru;
      break;
    case 't' :
      returnValue = charLettert;
      break;
    case 'o' :
      returnValue = charLettero;
      break;
    case 'C' :
      returnValue = charLetterC;
      break;
    case 'i' :
      returnValue = charLetteri;
      break;
  }
  return returnValue;
}


void eraseExternalTemp (void) {
  uint16_t fixedXlocation = 50;
  uint16_t fixedYlocation = 50;
  uint16_t xSize = 50;
  uint16_t ySize = 50;

  // tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, YELLOW);
}


void drawExternalTemp (char tens, char units) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;
  uint16_t fixedXlocation = 50;
  uint16_t fixedYlocation = 50;

  // write out the temperature
  bitmapTextSize = 12;
  bitmapTextGapSize = 3;

}


void eraseDemandTemp (void) {
  uint16_t fixedXlocation = 150;
  uint16_t fixedYlocation = 180;
  uint16_t xSize = 230;
  uint16_t ySize = 102;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawDemandTemp (char tens, char units, char half) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;
  uint16_t fixedXlocation = 150;
  uint16_t fixedYlocation = 180;
  
  // write out the temperature
  bitmapTextSize = 12;
  bitmapTextGapSize = 3;
  newXlocation = fixedXlocation;
  newYlocation = fixedYlocation;
  newXlocation = drawBitmapChar(lookupChar(tens), bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  newXlocation = drawBitmapChar(lookupChar(units), bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  // write out the half temperature
  bitmapTextSize = 6;
  bitmapTextGapSize = 1;
  newYlocation = fixedYlocation + 7 * 12 + 6 * 3 - ( 7 * 6 + 6 * 1 );
  if ('0' == half) {
    newXlocation = drawBitmapChar(charDecimal, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
    newXlocation = drawBitmapChar(charNumeral0, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  }
  else {
    newXlocation = drawBitmapChar(charDecimal, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
    newXlocation = drawBitmapChar(charNumeral5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  }  
}


void eraseAuto (void) {
  uint16_t fixedXlocation = 160;
  uint16_t fixedYlocation = 130;
  uint16_t xSize = 102;
  uint16_t ySize = 36;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawAuto (char inChar) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;
  uint16_t fixedXlocation = 160;
  uint16_t fixedYlocation = 130;

  if ('a' == inChar) {
    // write out auto
    bitmapTextSize = 4;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charLetterA, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
    newXlocation = drawBitmapChar(charLetteru, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
    newXlocation = drawBitmapChar(charLettert, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
    newXlocation = drawBitmapChar(charLettero, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  }
}


void eraseDegreeSymbol (void) {
  uint16_t fixedXlocation = 275;
  uint16_t fixedYlocation = 180;
  uint16_t xSize = 10;
  uint16_t ySize = 10;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, RED);
}


void drawDegreeSymbol (void) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;
  
  // write out degrees C
  newXlocation = 275;
  newYlocation = 180;
  newXlocation = drawBitmapChar(charDegree, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
  newXlocation = drawBitmapChar(charLetterC, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE);
}


void eraseFanSymbol (void) {
  uint16_t fixedXlocation = 5;
  uint16_t fixedYlocation = 200;
  uint16_t xSize = ((2 * 5) + (1 * 4)) * 3 + 2 * 1;
  uint16_t ySize = ((2 * 7) + (1 * 6)) * 2 + 2 * 1;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawFanSymbol (char inChar) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;  
  uint16_t fixedXlocation = 5;
  uint16_t fixedYlocation = 200;

  if ('F' == inChar) {
    // draw fan
    bitmapTextSize = 2;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charFanA1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charFanA2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charFanA3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charFanB1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charFanB2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charFanB3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
  }
}


void eraseFanBlock (void) {
  uint16_t fixedXlocation = 15;
  uint16_t fixedYlocation = 180;
  uint16_t xSize = 4 * 12;
  uint16_t ySize = -10 * 12;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, MAGENTA);
}


void drawFanBlock (char inChar1, char inChar2) {
  uint16_t fixedXlocation = 15;
  uint16_t fixedYlocation = 180;
  uint8_t shapeSize;

  if (inChar1) {
    shapeSize = inChar2 - 48; // 48 is decimal ascii for '0'
    tft.fillTriangle(fixedXlocation, fixedYlocation, fixedXlocation, fixedYlocation - (10 * shapeSize), fixedXlocation + (4 * shapeSize), fixedYlocation - (10 * shapeSize), WHITE); //tft.color565(0, i, i));
  }
}


void eraseArrowRSymbol (void) {
  uint16_t fixedXlocation = 350;
  uint16_t fixedYlocation = 160;
  uint16_t xSize = ((2 * 5) + (1 * 4)) * 3 + 2 * 1;
  uint16_t ySize = ((2 * 7) + (1 * 6)) * 2 + 2 * 1;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawArrowRSymbol (char inChar) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;  
  uint16_t fixedXlocation = 350;
  uint16_t fixedYlocation = 160;

  if (('f' == inChar) || ('b' == inChar)) {
    // draw arrow right
    bitmapTextSize = 2;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charArrowRA1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRA2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRA3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charArrowRB1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRB2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRB3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
  }
}


void eraseArrowRDSymbol (void) {
  uint16_t fixedXlocation = 350;
  uint16_t fixedYlocation = 160;
  uint16_t xSize = ((2 * 5) + (1 * 4)) * 3 + 2 * 1;
  uint16_t ySize = ((2 * 7) + (1 * 6)) * 2 + 2 * 1;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawArrowRDSymbol (char inChar) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;  
  uint16_t fixedXlocation = 350;
  uint16_t fixedYlocation = 160;

  if (('f' == inChar) || ('b' == inChar)) {
    // draw arrow right down
    bitmapTextSize = 2;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charArrowRDA1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRDA2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRDA3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charArrowRDB1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRDB2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charArrowRDB3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
  }
}


void eraseScreenSymbol (void) {
  uint16_t fixedXlocation = 330;
  uint16_t fixedYlocation = 30;
  uint16_t xSize = ((2 * 5) + (1 * 4)) * 3 + 2 * 1;
  uint16_t ySize = ((2 * 7) + (1 * 6)) * 2 + 2 * 1;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawScreenSymbol (char inChar) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;  
  uint16_t fixedXlocation = 330;
  uint16_t fixedYlocation = 30;

  if (('s' == inChar) || ('b' == inChar)) {
    // draw screeen
    bitmapTextSize = 2;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charScreenA1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charScreenA2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charScreenA3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charScreenB1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charScreenB2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
    newXlocation = drawBitmapChar(charScreenB3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, WHITE) - bitmapTextSize;
  }
}


void eraseDriverSymbol (void) {
  uint16_t fixedXlocation = 380;
  uint16_t fixedYlocation = 40;
  uint16_t xSize = ((2 * 5) + (1 * 4)) * 6 + 5 * 1;
  uint16_t ySize = ((2 * 7) + (1 * 6)) * 6 + 5 * 1;

  tft.fillRect(fixedXlocation, fixedYlocation, xSize, ySize, BLACK);
}


void drawDriverSymbol (char inChar1, char inChar2) {
  uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;  
  uint16_t fixedXlocation = 380;
  uint16_t fixedYlocation = 40;
  uint16_t faceColour = GREY;
  uint16_t feetColour = GREY;

  if (('f' == inChar1) || ('b' == inChar1)) {
    faceColour = WHITE;
  }

  if (('f' == inChar2) || ('b' == inChar2)) {
    feetColour = WHITE;
  }
  
  if (1) {
    // draw person
    bitmapTextSize = 2;
    bitmapTextGapSize = 1;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation;
    newXlocation = drawBitmapChar(charPersonA1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonA2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonA3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonA4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonA5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonA6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charPersonB1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonB2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonB3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonB4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonB5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonB6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, faceColour) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + 2 * (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charPersonC1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonC2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonC3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonC4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonC5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonC6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + 3 * (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charPersonD1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonD2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonD3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonD4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonD5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonD6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, GREY) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + 4 * (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charPersonE1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonE2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonE3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonE4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonE5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonE6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = fixedXlocation;
    newYlocation = fixedYlocation + 5 * (bitmapTextSize + bitmapTextGapSize) * 7;
    newXlocation = drawBitmapChar(charPersonF1, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonF2, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonF3, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonF4, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonF5, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
    newXlocation = drawBitmapChar(charPersonF6, bitmapTextSize, bitmapTextGapSize, newXlocation, newYlocation, feetColour) - bitmapTextSize;
  }
}


void setup(void) {
    Serial.begin(38400);
    Serial.setTimeout(9);  // timeout is in milliseconds
    uint32_t when = millis();
    //    while (!Serial) ;   //hangs a Leonardo until you connect a Serial
    if (!Serial) delay(5000);           //allow some time for Leonardo
    // Serial.println("Serial took " + String((millis() - when)) + "ms to start");
    //    tft.reset();                 //hardware reset
    uint16_t ID = tft.readID(); //
    Serial.print("ID = 0x");
    Serial.println(ID, HEX);
    if (ID == 0xD3D3) ID = 0x9481; // write-only shield
//    ID = 0x9329;                             // force ID
    tft.begin(ID);
}


void printmsg(int row, const char *msg)
{
    tft.setTextColor(YELLOW, BLACK);
    tft.setCursor(0, row);
    tft.println(msg);
}


void loop(void) {
    uint8_t aspect;
    uint16_t pixel, newXlocation, newYlocation, bitmapTextSize, bitmapTextGapSize;
    const char *aspectname[] = {
        "PORTRAIT", "LANDSCAPE", "PORTRAIT_REV", "LANDSCAPE_REV"
    };
    const char *colorname[] = { "BLUE", "GREEN", "RED", "GRAY" };
    uint16_t colormask[] = { 0x001F, 0x07E0, 0xF800, 0xFFFF };
    uint16_t dx, rgb, n, wid, ht, msglin;
    unsigned long last_millis_I_saw_a_data_rcvd;
    
    enum machineStateEnum {
      idle,
      bytes,
      munge,
      cleanup,
      clockset};

    enum machineStateEnum machineState;
    String returnString;
    String oldString;
    bool inputFromTouchScreen = 0;

    tft.setRotation(1);

    // ----- run inbuilt tests for green on black flash screen
    runtests();
    
    delay(500);

    if (tft.height() > 64) {

#ifdef DEADBEEF      
// --------------------------------------- do this four times changing the rotation and colours
//        for (uint8_t cnt = 0; cnt < 4; cnt++) {
     for (uint8_t cnt = 1; cnt < 2; cnt++) {  // just do it once, thanks, and in landscape mode
       aspect = (cnt + 0) & 3;
       tft.setRotation(aspect);
       wid = tft.width();
       ht = tft.height();
       msglin = (ht > 160) ? 200 : 112;

       // ------ print out a load of text which we have saved in a function
       testText();

       // ------ draw a load of rectangles of different colour intensities
       dx = wid / 32;
       for (n = 0; n < 32; n++) {
         rgb = n * 8;
         rgb = tft.color565(rgb, rgb, rgb);
         tft.fillRect(n * dx, 48, dx, 63, rgb & colormask[aspect]);
       }

            
       tft.drawRect(0, 48 + 63, wid, 1, WHITE);
       tft.setTextSize(2);
       tft.setTextColor(colormask[aspect], BLACK);

       // ------ print text colour and "colour grades" over the top of the colour bars
       tft.setCursor(0, 72);
       tft.print(colorname[aspect]);
       tft.setTextColor(WHITE);
       tft.println(" COLOR GRADES");
       tft.setTextColor(WHITE, BLACK);

       // ------ down the bottom, print the aspect
       printmsg(184, aspectname[aspect]);
       delay(500);

       // ------ draw a pixel in the corner of the screen
       tft.drawPixel(0, 0, YELLOW);

       // ------- read the colour back
       pixel = tft.readPixel(0, 0);

       // ------- set the text size for messages
       tft.setTextSize((ht > 160) ? 2 : 1); //for messages
#endif

#if defined(MCUFRIEND_KBV_H_)

       tft.fillScreen(BLACK);

       returnString = "14--a042-A";
       machineState = munge;  // start off in the idle state
       last_millis_I_saw_a_data_rcvd = millis()-100;  // make sure that the timeout triggers

       while (1) {

         switch (machineState) {
  
           case idle :
             // when in the idle state, sit around until we see a first byte of the data packet
             if (Serial.available())
             {
               last_millis_I_saw_a_data_rcvd = millis();   // timestamp the receipt
               machineState = bytes;  // get the data from the uart
             }
             break;

           case bytes :
             // now pull the message from the serial FIFO using the timeout to detect the end of a message
             returnString = Serial.readString();
             returnString.trim();
             // check we picked up the whole message, not just the tail end or a truncated message
             if (9 == returnString.length())
             {
               machineState = munge;  // move to doing something with the input byte
             }
             break;
             
           case munge :
             /*
             if (oldString != returnString) {
               tft.fillScreen(BLACK);
               drawExternalTempTens (returnString.charAt(1));  // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L];
               drawExternalTempUnits (returnString.charAt(0));  // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L];
               drawArrowRSymbol (returnString.charAt(2));  // '-' = off, 's' = screen, 'f' = face, 'b' = both
               drawArrowRDSymbol (returnString.charAt(3));  // '-' = off, 'm' = man, 'f' = feet, 'b' = both
               drawAuto (returnString.charAt(4));  // '-' = off, 'a' = auto
               drawHalfDegree (returnString.charAt(5));  // '-' = off, '0' = x.0, '5' = x.5
               drawDemandTempTens (returnString.charAt(6));    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L]
               drawDemandTempUnits (returnString.charAt(7));    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L]
               drawFanSymbol (returnString.charAt(8));  // '-' = off, 'F' = fan
             }
             */
             if ((returnString.charAt(1) != oldString.charAt(1)) || (returnString.charAt(0) != oldString.charAt(0))) {
               eraseExternalTemp ();
               drawExternalTemp (returnString.charAt(1), returnString.charAt(0));  // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L]
             }
             if ((returnString.charAt(2) != oldString.charAt(2)) || (returnString.charAt(3) != oldString.charAt(3))) {
               eraseScreenSymbol ();
               drawScreenSymbol (returnString.charAt(2));  // '-' = off, 's' = screen, 'f' = face, 'b' = both
               eraseDriverSymbol ();
               drawDriverSymbol (returnString.charAt(2), returnString.charAt(3));    // '-' = off, 'm' = man, 'f' = feet, 'b' = both
             }
             if (returnString.charAt(4) != oldString.charAt(4)) {
               eraseAuto ();
               drawAuto (returnString.charAt(4));  // '-' = off, 'a' = auto
             }
             if ((returnString.charAt(6) != oldString.charAt(6)) || (returnString.charAt(7) != oldString.charAt(7)) || (returnString.charAt(5) != oldString.charAt(5))) {
               eraseDemandTemp ();
               drawDemandTemp (returnString.charAt(7), returnString.charAt(6), returnString.charAt(5));    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, H, L]
             }
             if (returnString.charAt(8) != oldString.charAt(8)) {
               eraseFanSymbol ();
               drawFanSymbol (returnString.charAt(8));  // '-' = off, 'F' = fan
             }
    //         if ((returnString.charAt(9) != oldString.charAt(9)) || (returnString.charAt(8) != oldString.charAt(8))) {
    //           eraseFanBlock ();
    //           drawFanBlock (returnString.charAt(8), returnString.charAt(9));  // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B]
    //         }
             machineState = cleanup;
             break;

           case cleanup : 
             oldString = returnString;
             machineState = idle;
             break;
             
           case clockset :
             break;
           };
       }



            delay(1000);

#endif // defined(MCUFRIEND_KBV_H_)


    }


      delay(500);
}


typedef struct {
    PGM_P msg;
    uint32_t ms;
} TEST;
TEST result[12];

#define RUNTEST(n, str, test) { result[n].msg = PSTR(str); result[n].ms = test; delay(500); }


void runtests(void)
{
    uint8_t i, len = 24, cnt;
    uint32_t total;

    #ifdef DO_TESTS
    RUNTEST(0, "FillScreen               ", testFillScreen());
    RUNTEST(1, "Text                     ", testText());
    RUNTEST(2, "Lines                    ", testLines(CYAN));
    RUNTEST(3, "Horiz/Vert Lines         ", testFastLines(RED, BLUE));
    RUNTEST(4, "Rectangles (outline)     ", testRects(GREEN));
    RUNTEST(5, "Rectangles (filled)      ", testFilledRects(YELLOW, MAGENTA));
    RUNTEST(6, "Circles (filled)         ", testFilledCircles(10, MAGENTA));
    RUNTEST(7, "Circles (outline)        ", testCircles(10, WHITE));
    RUNTEST(8, "Triangles (outline)      ", testTriangles());
    RUNTEST(9, "Triangles (filled)       ", testFilledTriangles());
    RUNTEST(10, "Rounded rects (outline)  ", testRoundRects());
    RUNTEST(11, "Rounded rects (filled)   ", testFilledRoundRects());
    #endif //DO_TESTS

    tft.fillScreen(BLACK);
    tft.setTextColor(GREEN);
    tft.setCursor(0, 0);
    uint16_t wid = tft.width();
    if (wid > 176) {
        tft.setTextSize(2);
#if defined(MCUFRIEND_KBV_H_)
        tft.print("MCUFRIEND ");
#if MCUFRIEND_KBV_H_ != 0
        tft.print(0.01 * MCUFRIEND_KBV_H_, 2);
#else
        tft.print("for");
#endif
        tft.println(" UNO");
#else
        tft.println("Adafruit-Style Tests");
#endif
    } else len = wid / 6 - 8;
    
    tft.setTextSize(1);
    total = 0;
    for (i = 0; i < 12; i++) {
        PGM_P str = result[i].msg;
        char c;
        if (len > 24) {
            if (i < 10) tft.print(" ");
            tft.print(i);
            tft.print(": ");
        }
        uint8_t cnt = len;
        while ((c = pgm_read_byte(str++)) && cnt--) tft.print(c);
        tft.print(" ");
        tft.println(result[i].ms);
        total += result[i].ms;
    }
    tft.setTextSize(2);
    tft.print("Total:");
    tft.print(0.000001 * total);
    tft.println("sec");
    g_identifier = tft.readID();
    tft.print("ID: 0x");
    tft.println(tft.readID(), HEX);
//    tft.print("Reg(00):0x");
//    tft.println(tft.readReg(0x00), HEX);
    tft.print("F_CPU:");
    tft.print(0.000001 * F_CPU);
#if defined(__OPTIMIZE_SIZE__)
    tft.println("MHz -Os");
#else
    tft.println("MHz");
#endif

    delay(500);
}


boolean testBit(uint8_t byteIn, uint8_t offset)
{
  uint8_t mask = 1 << offset;
  return (boolean)(byteIn & mask);
}


uint8_t setBit(uint8_t byteIn, uint8_t offset)
{
  uint8_t mask = 1 << offset;
  return (byteIn | mask);
}


uint16_t skipBitmapChar(uint8_t character, uint8_t squareSize, uint8_t gapSize, uint16_t Xlocation, uint16_t Ylocation, uint16_t colour)
{
  extern uint8_t fontWidth;
  extern uint8_t fontHeight;
  extern uint8_t charWidthIndex;extern const uint8_t fontlcd[];
  uint16_t fontArrayOffset = character * 8;
  uint8_t characterWidth = pgm_read_byte_near(fontlcd + fontArrayOffset + charWidthIndex);
  return Xlocation + characterWidth * (squareSize + gapSize) + squareSize; 
}


uint16_t drawBitmapChar(uint8_t character, uint8_t squareSize, uint8_t gapSize, uint16_t Xlocation, uint16_t Ylocation, uint16_t colour)
{
  extern uint8_t fontWidth;
  extern uint8_t fontHeight;
  extern uint8_t charWidthIndex;

  extern const uint8_t fontlcd[];

  uint16_t fontArrayOffset = character * 8;
  uint8_t characterWidth = pgm_read_byte_near(fontlcd + fontArrayOffset + charWidthIndex);
  uint8_t rowCounter;
  uint8_t squareCounter;

  uint16_t qwerty = 0;

  // ******** BEFORE YOU DO ANYTHING, CHECK THAT THE CHARACTER WILL NOT FALL OFF THE END OF THE SCREEN *****
  ///  INSERT CODE HERE

  for(rowCounter = 0; fontHeight > rowCounter; rowCounter++)
    {       
       for(squareCounter = 0; characterWidth > squareCounter; squareCounter++)
         {
            if(testBit(pgm_read_byte_near(fontlcd + fontArrayOffset + rowCounter), characterWidth - squareCounter - 1))
              {
                 tft.fillRect(Xlocation + squareCounter * (squareSize + gapSize), Ylocation  + rowCounter * (squareSize + gapSize), squareSize, squareSize, colour);
              }
         }
    }
  return Xlocation + characterWidth * (squareSize + gapSize) + squareSize;
}

 
// Standard Adafruit tests.  will adjust to screen size

unsigned long testFillScreen() {
    unsigned long start = micros();
    tft.fillScreen(BLACK);
    tft.fillScreen(RED);
    tft.fillScreen(GREEN);
    tft.fillScreen(BLUE);
    tft.fillScreen(BLACK);
    return micros() - start;
}


unsigned long testText() {
    unsigned long start;
    tft.fillScreen(BLACK);
    start = micros();
    tft.setCursor(0, 0);
    tft.setTextColor(WHITE);  tft.setTextSize(1);
    tft.println("Hello World!");
    tft.setTextColor(YELLOW); tft.setTextSize(2);
    tft.println(123.45);
    tft.setTextColor(RED);    tft.setTextSize(3);
    tft.println(0xDEADBEEF, HEX);
    tft.println();
    tft.setTextColor(GREEN);
    tft.setTextSize(5);
    tft.println("Groop");
    tft.setTextSize(2);
    tft.println("I implore thee,");
    tft.setTextSize(1);
    tft.println("my foonting turlingdromes.");
    tft.println("And hooptiously drangle me");
    tft.println("with crinkly bindlewurdles,");
    tft.println("Or I will rend thee");
    tft.println("in the gobberwarts");
    tft.println("with my blurglecruncheon,");
    tft.println("see if I don't!");
    return micros() - start;
}


unsigned long testLines(uint16_t color) {
    unsigned long start, t;
    int           x1, y1, x2, y2,
                  w = tft.width(),
                  h = tft.height();

    tft.fillScreen(BLACK);

    x1 = y1 = 0;
    y2    = h - 1;
    start = micros();
    for (x2 = 0; x2 < w; x2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    x2    = w - 1;
    for (y2 = 0; y2 < h; y2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    t     = micros() - start; // fillScreen doesn't count against timing

    tft.fillScreen(BLACK);

    x1    = w - 1;
    y1    = 0;
    y2    = h - 1;
    start = micros();
    for (x2 = 0; x2 < w; x2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    x2    = 0;
    for (y2 = 0; y2 < h; y2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    t    += micros() - start;

    tft.fillScreen(BLACK);

    x1    = 0;
    y1    = h - 1;
    y2    = 0;
    start = micros();
    for (x2 = 0; x2 < w; x2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    x2    = w - 1;
    for (y2 = 0; y2 < h; y2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    t    += micros() - start;

    tft.fillScreen(BLACK);

    x1    = w - 1;
    y1    = h - 1;
    y2    = 0;
    start = micros();
    for (x2 = 0; x2 < w; x2 += 6) tft.drawLine(x1, y1, x2, y2, color);
    x2    = 0;
    for (y2 = 0; y2 < h; y2 += 6) tft.drawLine(x1, y1, x2, y2, color);

    return micros() - start;
}


unsigned long testFastLines(uint16_t color1, uint16_t color2) {
    unsigned long start;
    int           x, y, w = tft.width(), h = tft.height();

    tft.fillScreen(BLACK);
    start = micros();
    for (y = 0; y < h; y += 5) tft.drawFastHLine(0, y, w, color1);
    for (x = 0; x < w; x += 5) tft.drawFastVLine(x, 0, h, color2);

    return micros() - start;
}


unsigned long testRects(uint16_t color) {
    unsigned long start;
    int           n, i, i2,
                  cx = tft.width()  / 2,
                  cy = tft.height() / 2;

    tft.fillScreen(BLACK);
    n     = min(tft.width(), tft.height());
    start = micros();
    for (i = 2; i < n; i += 6) {
        i2 = i / 2;
        tft.drawRect(cx - i2, cy - i2, i, i, color);
    }

    return micros() - start;
}


unsigned long testFilledRects(uint16_t color1, uint16_t color2) {
    unsigned long start, t = 0;
    int           n, i, i2,
                  cx = tft.width()  / 2 - 1,
                  cy = tft.height() / 2 - 1;

    tft.fillScreen(BLACK);
    n = min(tft.width(), tft.height());
    for (i = n; i > 0; i -= 6) {
        i2    = i / 2;
        start = micros();
        tft.fillRect(cx - i2, cy - i2, i, i, color1);
        t    += micros() - start;
        // Outlines are not included in timing results
        tft.drawRect(cx - i2, cy - i2, i, i, color2);
    }

    return t;
}


unsigned long testFilledCircles(uint8_t radius, uint16_t color) {
    unsigned long start;
    int x, y, w = tft.width(), h = tft.height(), r2 = radius * 2;

    tft.fillScreen(BLACK);
    start = micros();
    for (x = radius; x < w; x += r2) {
        for (y = radius; y < h; y += r2) {
            tft.fillCircle(x, y, radius, color);
        }
    }

    return micros() - start;
}


unsigned long testCircles(uint8_t radius, uint16_t color) {
    unsigned long start;
    int           x, y, r2 = radius * 2,
                        w = tft.width()  + radius,
                        h = tft.height() + radius;

    // Screen is not cleared for this one -- this is
    // intentional and does not affect the reported time.
    start = micros();
    for (x = 0; x < w; x += r2) {
        for (y = 0; y < h; y += r2) {
            tft.drawCircle(x, y, radius, color);
        }
    }

    return micros() - start;
}


unsigned long testTriangles() {
    unsigned long start;
    int           n, i, cx = tft.width()  / 2 - 1,
                        cy = tft.height() / 2 - 1;

    tft.fillScreen(BLACK);
    n     = min(cx, cy);
    start = micros();
    for (i = 0; i < n; i += 5) {
        tft.drawTriangle(
            cx    , cy - i, // peak
            cx - i, cy + i, // bottom left
            cx + i, cy + i, // bottom right
            tft.color565(0, 0, i));
    }

    return micros() - start;
}


unsigned long testFilledTriangles() {
    unsigned long start, t = 0;
    int           i, cx = tft.width()  / 2 - 1,
                     cy = tft.height() / 2 - 1;

    tft.fillScreen(BLACK);
    start = micros();
    for (i = min(cx, cy); i > 10; i -= 5) {
        start = micros();
        tft.fillTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
                         tft.color565(0, i, i));
        t += micros() - start;
        tft.drawTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
                         tft.color565(i, i, 0));
    }

    return t;
}


unsigned long testRoundRects() {
    unsigned long start;
    int           w, i, i2, red, step,
                  cx = tft.width()  / 2 - 1,
                  cy = tft.height() / 2 - 1;

    tft.fillScreen(BLACK);
    w     = min(tft.width(), tft.height());
    start = micros();
    red = 0;
    step = (256 * 6) / w;
    for (i = 0; i < w; i += 6) {
        i2 = i / 2;
        red += step;
        tft.drawRoundRect(cx - i2, cy - i2, i, i, i / 8, tft.color565(red, 0, 0));
    }

    return micros() - start;
}


unsigned long testFilledRoundRects() {
    unsigned long start;
    int           i, i2, green, step,
                  cx = tft.width()  / 2 - 1,
                  cy = tft.height() / 2 - 1;

    tft.fillScreen(BLACK);
    start = micros();
    green = 256;
    step = (256 * 6) / min(tft.width(), tft.height());
    for (i = min(tft.width(), tft.height()); i > 20; i -= 6) {
        i2 = i / 2;
        green -= step;
        tft.fillRoundRect(cx - i2, cy - i2, i, i, i / 8, tft.color565(0, green, 0));
    }

    return micros() - start;
}
