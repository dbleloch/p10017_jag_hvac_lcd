import os
import numpy
from scipy import misc
path = 'C:\Users\david\Documents\Arduino\sketch_apr02a'

# testBit() returns a nonzero result, 2**offset, if the bit at 'offset' is one.

def testBit(int_type, offset):
    mask = 1 << offset
    return(int_type & mask)

# setBit() returns an integer with the bit at 'offset' set to 1.

def setBit(int_type, offset):
    mask = 1 << offset
    return(int_type | mask)

# clearBit() returns an integer with the bit at 'offset' cleared.

def clearBit(int_type, offset):
    mask = ~(1 << offset)
    return(int_type & mask)

# toggleBit() returns an integer with the bit at 'offset' inverted, 0 -> 1 and 1 -> 0.

def toggleBit(int_type, offset):
    mask = 1 << offset
    return(int_type ^ mask)
    
# get the image into memory    
image= misc.imread(os.path.join(path,'icon.bmp'), flatten= 0)
## flatten=0 if image is required as it is 
## flatten=1 to flatten the color layers into a single gray-scale layer

# creat a blank array
newimage=numpy.empty([40,40,2])

for column in range(40):
    for pixel in range(40):
        red = int(image[column][pixel][0])
        green = int(image[column][pixel][1])
        blue = int(image[column][pixel][2])
        ## newimage[column][pixel][0] = (blue & 0xF8) | ((green & 0xE0)>> 5)
        ## newimage[column][pixel][1] = ((green & 0x1C)<< 3) | ((red & 0xF8)>> 3)
        newimage[column][pixel][0] = 0
        newimage[column][pixel][1] = 0
        if testBit(green, 7):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),2)
        if testBit(green, 6):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),1)
        if testBit(green, 5):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),0)
        if testBit(green, 4):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),7)
        if testBit(green, 3):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),6)
        if testBit(green, 2):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),5)
        if testBit(blue, 7):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),4)
        if testBit(blue, 6):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),3)
        if testBit(blue, 5):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),2)
        if testBit(blue, 4):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),1)
        if testBit(blue, 3):
            newimage[column][pixel][0] = setBit(int(newimage[column][pixel][0]),0)
        if testBit(red, 7):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),7)
        if testBit(red, 6):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),6)
        if testBit(red, 5):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),5)
        if testBit(red, 4):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),4)
        if testBit(red, 3):
            newimage[column][pixel][1] = setBit(int(newimage[column][pixel][1]),3)
        
fileLineCounter = 0

with open(os.path.join(path,'newicon.c'), 'w') as f:
    f.write('#if defined(__arm__) || defined(ESP8266) || defined(ESP32)\n')
    f.write('#define PROGMEM\n')
    f.write('#else \n')
    f.write('#include <Arduino.h>\n')
    f.write('#endif\n')
    f.write('\n')
    f.write('const unsigned char PROGMEM icon[3200] = {\n')
    for column in range(40):
        for pixel in range(40):
            if fileLineCounter == 0:
                f.write('    ')
            f.write("0x{:02x}".format(int(newimage[column][pixel][0])))
            f.write(', ')
            f.write("0x{:02x}".format(int(newimage[column][pixel][1])))
            f.write(', ')
            fileLineCounter = fileLineCounter + 1
            if fileLineCounter == 8:
                f.write('\n')
                fileLineCounter = 0
    f.writelines('};')
