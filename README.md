p10017_Jag LCD_HVAC_LCD

The end goal of this work is to replace the LCD that is used within the Jaguar X Type automatic HVAC unit with a TFT and to add extra functionality.

Initial stuff to go in the repo :
1. Report on the initial investigation including teardown of the unit, photos of the modifications, screenshots of the data, list of equipment used
2. Raw data example files
3. Processed data files
4. Processing script example
5. Analysis spreadsheet showing the structure of messages and how they are assembled

Future stuff to go in the repo :
1. Design for a data extraction PCB (most likely based on a Digispark ATTiny85 with code in C under Atmel Studio
2. Supporting datapack documenting how to interface to the HVAC and the data output format
3. Design for a display PCB with TFT daughterboard (most likely based on an Arduino Leonardo with code in C under Atmel Studio
4. Supporting datapack document

Future expansion :
The partitioning between a "sniffer board" and a "display board" will allow some flexibility in just implementing one half of the design.
This might be because a user might just want to grab the data from the LCD and drive his/her own display or input the data into a different footprint (Android?)
An additional design might follow to sample the buttons on the HVAC unit and serialise the data into the sniffer board.
This will allow the buttons to be sampled and sent efficiently to another destination
An additional desing might follow to receive a serialised data stream and drive the button inputs on the HVAC control PCB, removing the unit from the console.



------------------------------------------------------------------------------------


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).